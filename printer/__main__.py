import asyncio

import websockets

SERVER_IP = "10.7.43.29"

async def main():
    # Connect to websocket
    async with websockets.connect(f"ws://{SERVER_IP}:8000/ws") as websocket:
        print("Connected")
        while True:
            order_id = await websocket.recv()
            print("New order received: ", order_id)

            # Start Printing here
            # ...
            # End Printing here (oda a ned wenn des a fire and forget Anweisung is)


if __name__ == "__main__":
    asyncio.new_event_loop().run_until_complete(main())
