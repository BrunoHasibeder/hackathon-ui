import asyncio
import json

import websockets

SERVER_IP = "10.7.43.29"

current_order_id = ""


async def main():
    # Connect to websocket
    async with websockets.connect(f"ws://{SERVER_IP}:8000/ws") as websocket:
        print("Connected")
        while True:
            global current_order_id
            current_order_id = await websocket.recv()
            print("New order: ", current_order_id)

            # Start FTS here
            # ...
            # Example:
            await websocket.send(json.dumps({
                "id": current_order_id,
                "progress": 0.2  # Update the progress to be 20%
            }))
            # ...
            await websocket.send(json.dumps({
                "id": current_order_id,
                "container": 1  # Update the currently loading container
            }))
            # ...
            # End FTS here


if __name__ == "__main__":
    asyncio.new_event_loop().run_until_complete(main())
