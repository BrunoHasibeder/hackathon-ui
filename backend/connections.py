from __future__ import annotations

import json
from typing import List

from starlette.websockets import WebSocket, WebSocketDisconnect

from orders import orders

connections: List[WebSocketConnection] = []


class WebSocketConnection():
    def __init__(self, websocket: WebSocket):
        self.websocket = websocket

    async def loop(self):
        '''
        This function never quits! Be careful

        :return:
        '''

        try:
            while True:
                request = await self.websocket.receive_text()
                request = json.loads(request)
                id = request["id"]

                print("Update order ", id, "to state", request)

                if "progress" in request:
                    orders[id].progress = request["progress"]

                if "container" in request:
                    orders[id].container_index = request["container"]
        except WebSocketDisconnect:
            print("WebSocket ", f"{self.websocket.client.host}:{self.websocket.client.port}", "disconnected")
        finally:
            connections.remove(self)
