import asyncio

import websockets


async def main():
    async with websockets.connect("ws://10.7.43.29:8000/ws") as websocket:
        print("Connected")
        while True:
            print(await websocket.recv())


if __name__ == "__main__":
    asyncio.new_event_loop().run_until_complete(main())
