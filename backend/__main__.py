from __future__ import annotations

import fcntl
import socket
import struct

import uvicorn

from api import app

LOCALHOST = False
IP = "10.7.43.29"


# Danke stackoverflow
def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', bytes(ifname[:15], 'utf-8'))
    )[20:24])


def main():
    uvicorn.run(app, host=get_ip_address("wlp2s0") if not LOCALHOST else "localhost")


if __name__ == '__main__':
    main()
