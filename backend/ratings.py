from typing import Any

from pydantic import BaseModel

ratings = {

}


class Rating(BaseModel):
    stars: int
    comment: str | None = None
    product: int

    def __init__(self, **data: Any):
        super().__init__(**data)
        global ratings
        if self.product in ratings:
            ratings[self.product].append(self)
        else:
            ratings[self.product] = [self]
