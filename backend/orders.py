from __future__ import annotations

import uuid
from typing import Any

from pydantic import BaseModel

orders: dict[str, Order] = {

}


class Order(BaseModel):
    id: str
    product_index: int
    container_index: int | None = None
    progress: float = 0.0

    def __init__(self, **data: Any):
        super().__init__(id=str(uuid.uuid4()), **data)
        global orders
        orders[self.id] = self
