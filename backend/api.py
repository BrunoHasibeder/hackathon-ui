from fastapi import FastAPI, Body
from fastapi.openapi.docs import get_swagger_ui_html
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import Response
from starlette.websockets import WebSocket

from connections import connections, WebSocketConnection
from orders import Order
from orders import orders
from ratings import Rating, ratings

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def running():
    return Response(
        "running"
    )


@app.post("/order")
async def submit_order(product: int = Body(embed=True, default=0)):
    order = Order(product_index=product)

    for connection in connections:
        await connection.websocket.send_text(order.json())

    return order


@app.get("/order/{id}")
def get_order(id: str):
    order = orders[id]

    return order


@app.get("/docs", include_in_schema=False)
def swagger():
    return get_swagger_ui_html(
        openapi_url="/openapi.json",
        title="FastAPI"
    )


@app.patch("/order/{id}")
def update_order(id: str,
                 progress: float | None = Body(None, embed=True), container: int | None = Body(None, embed=True)):
    order = orders[id]
    if progress is not None:
        order.progress = progress
    if container is not None:
        order.container_index = None if container < 0 else container


@app.websocket("/ws")
async def websocket(websocket: WebSocket):
    await websocket.accept()
    connection = WebSocketConnection(websocket)

    connections.append(connection)
    await connection.loop()


@app.post("/rating")
async def submit_rating(
        product: int = Body(embed=True, default=0),
        stars: int = Body(..., embed=True),
        comment: str | None = Body(None, embed=True)
):
    comment = None if comment is None or len(comment.strip(" ")) < 1 else comment
    rating = Rating(product=product, stars=stars, comment=comment)

    return rating


@app.get("/ratings")
async def get_ratings():
    temp = []
    for key in ratings:
        temp.extend(ratings[key])

    return temp


@app.get("/order/{id}")
def get_order(id: str):
    order = orders[id]

    return order
