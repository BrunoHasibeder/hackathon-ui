/** @type {import('tailwindcss').Config} */
const withMT = require("@material-tailwind/react/utils/withMT");

module.exports = withMT({
    content: [
        "./src/**/*.{js,jsx,ts,tsx}",
    ],
    theme: {
        extend: {
            colors: {
                fill: {
                    900: "#b60000"
                }
            }
        },
    },
    plugins: [
        require('@tailwindcss/line-clamp')
    ],
})
