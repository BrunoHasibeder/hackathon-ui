const paths = {
    home: "/",
    newOrder: getNewOrder,
    viewOrder: getViewOrder,
    goodbye: getGoodbye,
    filliFutureProduct: "/products/filli-future"
}

function getViewOrder(id: string) {
    return `/orders/${id}`
}

function getGoodbye(product: number) {
    return `/goodbye/${product}`
}

function getNewOrder(product: number) {
    return `/order/new?product=${product}`
}

export default paths
