import type {NextPage} from 'next'
import {observer} from "mobx-react";
import {useRouter} from "next/router";
import React, {useEffect, useState} from "react";
import axios from "axios";
import config from "../../config";
import paths from "../../util/paths";
import {Card} from "@material-tailwind/react";

let redirector: (() => Promise<void>) | undefined = undefined
let counter = 0

const NewOrder: NextPage = observer(() => {
    const router = useRouter()

    const sendOrder = async () => {
        return await axios.post(`http://${config().backendIP}:8000/order`, {
            product: typeof (router.query?.product) === "string" ? parseInt(router.query?.product) : 0
        })
            .then((r) => {
                console.log("New order: ", r.data.id)
                return r.data.id
            })
    }

    const handleOrder = async () => {
        if (!router.isReady) return

        const id = await sendOrder()
        redirector = async () => {
            console.log("Redirecting to order: ", id)
            await router.replace(paths.viewOrder(id))
        }
    }

    const [secRemaining, setSecRemaining] = useState(5)

    useEffect(() => {
        handleOrder()

        let remaining = 5
        const interval = setInterval(() => {
            if (redirector === undefined) return

            if (remaining-- <= 1) {
                redirector()
            }

            setSecRemaining(remaining)
        }, 1000)

        return () => clearInterval(interval)
    }, [router.isReady, router.query.slug])

    return <main className="flex place-content-center p-4 bg-gray-100 w-screen h-full">
        <Card className="flex flex-col space-y-8 p-8 pt-12 w-fit h-fit place-items-center">
            <div className="flex w-24 h-24 rounded-full border border-2 border-gray-400 place-content-center">
                <i className="h-fit my-auto fa-solid fa-check text-fill-900 text-4xl"/>
            </div>
            <div className="flex flex-col place-items-center space-y-2">
                <p className="text-fill-900 text-3xl font-sans leading-tight">Filli Future Successfully Ordered</p>
                <p className="text-gray-600 text-xl font-[450] leading-tight">Thank you very much for your trust!</p>
            </div>
            <div className="flex flex-col place-items-center">
                {secRemaining < 0 ?
                    <p>
                        Click
                        <button className="text-fill-900 underline px-1"
                                onClick={e => redirector ? redirector() : undefined}>
                            here
                        </button>
                        if you are still waiting...
                    </p> :
                    <p>You will be redirected in <span className="text-fill-900">{secRemaining} seconds</span></p>
                }
            </div>
        </Card>
    </main>
})

export default NewOrder
