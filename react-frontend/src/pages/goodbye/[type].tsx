import {NextPage} from "next";
import {observer} from "mobx-react";
import useWindowDimensions from "../../libs/useDimensions";
import {useRouter} from "next/router";
import React, {Fragment, ReactNode, useEffect, useState} from "react";
import Head from "next/head";
import {Accordion, AccordionBody, AccordionHeader, Button, Card, Textarea} from "@material-tailwind/react";
import {getProductDocumentation, Icon} from "../orders/[id]";
import {Document, Page, pdfjs} from "react-pdf";
import axios from "axios";
import config from "../../config";

const ViewGoodbye: NextPage = observer(() => {
    const router = useRouter()
    const [product, setProduct] = useState<number | undefined>(undefined)

    useEffect(() => {
        if (!router.isReady) return

        const product = typeof (router.query?.type) === "string" ? router.query.type : "0"
        setProduct(parseInt(product))
    }, [router.isReady])

    return <main className="flex p-4 bg-gray-100 h-full w-screen">
        <Head>
            <title>FILLer | Thanks</title>
        </Head>

        <Card className="p-4 w-full">
            {product !== undefined ?
                <GoodbyeContent product={product}/> :
                <p className="text-4xl place-self-center">Loading...</p>}
        </Card>
    </main>
})

const GoodbyeContent = observer((
    {product}:
        { product: number }
) => {
    const [open, setOpen] = useState(1)

    const handleOpen = (value: number) => {
        setOpen(open === value ? 0 : value);
    }

    return <Fragment>
        <p className="text-lg md:text-2xl lg:text-4xl font-medium text-gray-800 pb-2 lg:pb-4 leading-tight">
            Thank <span className="text-fill-900">you</span> for choosing us!
        </p>

        <p className="text-md md:text-lg lg:text-2xl font-normal text-gray-800">
            We sincerely hope you are satisfied with our service. Your feedback is greatly appreciated.
            You can also <DownloadPDF href={getProductDocumentation(product)}>
            download the documentation
        </DownloadPDF> or view it right here in the browser.
        </p>


        <Accordion open={open === 1} icon={<Icon open={open === 1}/>}>
            <AccordionHeader onClick={() => handleOpen(1)}>
                Feedback
            </AccordionHeader>
            <AccordionBody>
                <div className="flex w-full place-content-center">
                    <Feedback product={product}/>
                </div>
            </AccordionBody>
        </Accordion>
        <Accordion open={open === 2} icon={<Icon open={open === 2}/>}>
            <AccordionHeader onClick={e => handleOpen(2)}>
                Documentation
            </AccordionHeader>
            <div className={`collapse ${open == 2 ? "visible" : "hidden"}`}>
                <Documentation product={product}/>
            </div>
        </Accordion>
    </Fragment>
})

const Feedback = observer((
    {product}:
        { product: number }
) => {
    const [stars, setStars] = useState(5)
    const [comment, setComment] = useState("")
    const [submitted, setSubmitted] = useState(false)

    const handleSubmission = async () => {
        setSubmitted(true)

        await axios.post(`http://${config().backendIP}:8000/rating`, {
            product: product,
            stars: stars,
            comment: comment
        }).then((r) => {
            const data = r.data
            return data
        })
    }

    return <div className="flex flex-col space-y-8 place-items-center w-fit">
        {submitted ? <>
                <p className="text-lg md:text-xl lg:text-4xl text-gray-900 font-normal leading-tight">Thanks for the
                    feedback!</p>
            </> :
            <>
                <p className="text-lg md:text-xl lg:text-4xl text-gray-900 font-normal leading-tight">How would you rate
                    your
                    experience with us?</p>

                <div className="flex flex-col w-fit">
                    <div className="flex flex-row space-x-6">{Array.from(Array(5).keys()).map((index, _, array) => {
                        const Star = () => {
                            return <div className="flex flex-row place-content-center">
                                <button onClick={e => setStars(index + 1)}
                                        className="w-fit flex flex-row place-items-start">
                                    <i className={`${index < stars ? "fa-solid text-fill-900" : "fa-regular text-gray-600"} fa-star mx-auto`}/>
                                </button>
                            </div>
                        }

                        return <div className="text-lg md:text-2xl lg:text-4xl flex flex-col place-items-center">
                            <Star/>
                        </div>
                    })}</div>

                    <div
                        className="flex flex-row w-full place-content-between text-md md:text-xl lg:text-2xl font-normal text-gray-500">
                        <p className="pt-4">Poor</p>
                        <p className="pt-4">Excellent</p>
                    </div>
                </div>

                <div className="flex flex-col w-full">
                    <Textarea className="w-full" label="Additional comments" color="red" value={comment}
                              onChange={e => setComment(e.target.value)}/>
                </div>

                <Button className="bg-fill-900 lg:w-2/12" color="red" onClick={e => handleSubmission()}>
                    Submit
                </Button>
            </>}
    </div>
})

const Documentation = observer((
    {product}:
        { product: number }
) => {
    const dimensions = useWindowDimensions()

    const big = dimensions.width >= 1280

    pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
    return <div className="flex flex-row space-x-8">
        <Document file={getProductDocumentation(product)}>
            <Page pageNumber={1}/>
            {!big && <Page pageNumber={2}/>}
        </Document>
        {big && <Document file={getProductDocumentation(product)}>
            <Page pageNumber={2}/>
        </Document>}
    </div>
})

const DownloadPDF = observer((
    {href, children}:
        { href: string, children: ReactNode }
) => {
    return <div className="inline-flex flex-row text-fill-900 hover:underline text-md md:text-lg lg:text-2xl">
        <a href={href}
           className="hover:font-medium pr-2" download>
            {children}
        </a>
        <a href={href}
           className="group" target="_blank">
            <i className="fa-solid fa-external-link group-hover:border-b border-b-fill-900"/>
        </a>
    </div>
})

export default ViewGoodbye