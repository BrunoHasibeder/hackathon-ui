import type {NextPage} from 'next'
import {observer} from "mobx-react";
import {useRouter} from "next/router";
import React, {useEffect, useState} from "react";
import axios from "axios";
import config from "../../config";
import {Accordion, AccordionBody, AccordionHeader, Card, Progress} from "@material-tailwind/react";
import ParkourCanvas from "../../components/ParkourCanvas";
import useWindowDimensions from "../../libs/useDimensions";
import Head from "next/head";
import paths from "../../util/paths";

const ViewOrder: NextPage = observer(() => {
    const dimensions = useWindowDimensions()
    const router = useRouter()
    const {id} = router.query

    const [data, setData] = useState<{
        id: string, progress: number, container: number | null, productType: number
    } | undefined>(undefined)
    const [open, setOpen] = useState(1)
    const [finished, setFinished] = useState(false)
    const [secRemaining, setSecRemaining] = useState(5)

    const expanded = dimensions.width >= 1024
    const handleOpen = (value: number) => {
        setOpen(open === value ? 0 : value);
    }

    let date = new Date()

    const updateData = async () => {
        const data = await axios.get(`http://${config().backendIP}:8000/order/${id}`)
            .then((r) => {
                const response = r.data
                return {
                    id: response.id,
                    progress: response.progress,
                    container: response.container_index,
                    productType: response.product_index
                }
            })

        setData(data)
    }

    useEffect(() => {
        const interval = setInterval(() => {
            updateData()
        }, 500)

        return () => clearInterval(interval)
    }, [])

    useEffect(() => {
        const completed = (data?.progress ?? 0) >= 1
        if (!finished && completed) {
            setFinished(true)
        }
    }, [data])

    useEffect(() => {
        if (!finished) return

        let remaining = 5
        const interval = setInterval(() => {
            if (remaining-- <= 1) {
                router.push(paths.goodbye(data?.productType ?? 0))
            }

            setSecRemaining(remaining)
        }, 1000)

        return () => clearInterval(interval)
    }, [finished])

    return <main className="flex p-4 bg-gray-100 h-full w-screen">
        <Head>
            <title>FILLer | Order</title>
        </Head>

        <Card className="p-4 w-full">
            <div className="flex flex-col space-y-4 lg:space-y-0 lg:flex-row lg:place-content-between">
                <div className="flex flex-col lg:space-y-2">
                    <p className="text-lg md:text-1xl lg:text-4xl text-gray-900 font-medium leading-tight">Your
                        Order</p>
                    <p className="text-md md:text-lg lg:text-2xl text-fill-900/75 font-medium leading-tight">{id}</p>
                    {data && <p className="text-md md:text-lg lg:text-2xl font-normal leading-tight">
                        Product: <span className="font-bold">{getProductName(data.productType)}</span>
                    </p>}
                </div>
                {data &&
                    <>
                        <div className="flex flex-col lg:w-64 lg:space-y-2">
                            <p className="text-lg md:text-xl lg:text-4xl text-gray-800 font-medium leading-tight">Complete</p>
                            <p className="text-xl md:text-2xl lg:text-6xl text-gray-900 font-bold leading-tight">
                                {finished ? "Delivered" : Math.min(Math.round(data.progress * 100), 100) + "%"}
                            </p>
                            {finished ?
                                <div className="text-sm md:text-md lg:text-lg">{secRemaining < 0 ?
                                    <p>
                                        Click
                                        <button className="text-fill-900 underline px-1"
                                                onClick={e => router.push(paths.goodbye(data?.productType ?? 0))}>
                                            here
                                        </button>
                                        if still waiting...
                                    </p> :
                                    <p>Redirect in <span className="text-fill-900">{secRemaining} seconds</span></p>
                                } </div> :
                                <Progress className="w-full h-2 lg:h-3"
                                          value={Math.min(Math.round(data.progress * 100), 100)} color="red"/>
                            }
                        </div>

                        <div className="flex flex-col lg:text-right lg:space-y-2">
                            <p className="text-lg md:text-xl lg:text-4xl text-gray-800 font-medium leading-tight">Expected
                                Completion</p>
                            <p className="text-md md:text-lg lg:text-2xl text-gray-800 font-medium leading-tight">
                                {`${date.toLocaleString('default', {month: 'long'})} ${date.getDate()}, ${date.getFullYear()}`}
                            </p>
                            <p className="text-sm md:text-lg lg:text-xl text-gray-600 font-medium leading-tight">
                                {"< 1 hour"}
                            </p>
                        </div>
                    </>
                }
            </div>

            {!data ?
                <p className="mt-4 text-4xl place-self-center">Loading...</p> :
                <>
                    <div className="flex flex-col lg:flex-row lg:space-x-16">
                        <Accordion open={open === 1 || expanded} icon={<Icon open={open === 1} hidden={expanded}/>}>
                            <AccordionHeader onClick={expanded ? undefined : () => handleOpen(1)}>
                                Progress on Map
                            </AccordionHeader>
                            <AccordionBody>
                                <div className="h-screen">
                                    <ParkourCanvas progress={data.progress} container={data.container ?? null}/>
                                </div>
                            </AccordionBody>
                        </Accordion>
                        <Accordion open={open === 2 || expanded} icon={<Icon open={open === 2} hidden={expanded}/>}>
                            <AccordionHeader onClick={expanded ? undefined : () => handleOpen(2)}>
                                Realtime Camera
                            </AccordionHeader>
                            <AccordionBody>
                                <div className="w-full lg:w-9/12 h-screen">
                                    <img src={`http://${config().ftsIP}:8081/`} alt="Realtime Image"
                                         className="w-full"/>
                                </div>
                            </AccordionBody>
                        </Accordion>
                    </div>
                </>}
        </Card>
    </main>
})

export default ViewOrder

export function getProductName(productType: number) {
    switch (productType) {
        case 0:
            return "Standard Filli Future"
        case 1:
            return "Ninja-Edition Filli Future"
        default:
            return "Unknown Product"
    }
}

export function getProductDocumentation(productType: number) {
    switch (productType) {
        case 0:
            return "/ff-standard-documentation.pdf"
        case 1:
            return "/ff-ninja-documentation.pdf"
        default:
            return "/unknown.pdf"
    }
}

export function Icon({open, hidden}: { hidden?: boolean, open: boolean }) {
    return <>{
        !hidden &&
        <svg
            xmlns="http://www.w3.org/2000/svg"
            className={`${
                open ? "rotate-180" : ""
            } h-5 w-5 transition-transform`}
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            strokeWidth={2}
        >
            <path strokeLinecap="round" strokeLinejoin="round" d="M19 9l-7 7-7-7"/>
        </svg>
    }</>
}