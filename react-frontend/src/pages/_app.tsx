import '../styles/globals.css'
import "@fortawesome/fontawesome-free/css/all.css"
import type {AppProps} from 'next/app'
import {StoreProvider} from "../stores";
import Layout from "./_layout"
import {NextLayoutPage} from "../types/layout";
import {ReactElement} from "react";
import {ThemeProvider} from "@material-tailwind/react";
import theme from "../styles/theme";
import "../styles/filli-future.css";
import "../styles/react-pdf.css";

type AppLayoutProps = AppProps & {
    Component: NextLayoutPage
}

export default function MyApp({Component, pageProps}: AppLayoutProps) {
    const PageLayout = Component.Layout ?? (({children}: { children: ReactElement }) => children)

    return <StoreProvider>
        <ThemeProvider value={theme}>
            <Layout>
                <PageLayout>
                    <Component {...pageProps} />
                </PageLayout>
            </Layout>
        </ThemeProvider>
    </StoreProvider>
}
