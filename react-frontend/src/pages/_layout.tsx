import {ReactElement} from "react";
import Head from "next/head";
import {observer} from "mobx-react";
import Link from "next/link";
import paths from "../util/paths";

const Layout = observer(({children}: { children: ReactElement }) => {
    return <>
        <Head>
            <title>FILLer</title>
            <meta name="description"
                  content="Shopping website for Fill GmbH products"/>

            <link rel="shortcut icon" href="/favicon.ico"/>
            <link rel="icon" href="/favicon.ico"/>
        </Head>
        <div className="h-full">
            <div className="flex flex-row w-full h-16 lg:h-24 bg-white rounded-b-2xl place-content-between">
                <div className="flex flex-row w-64">
                    <img src="/fill-your-future.jpg" className="h-full"/>
                    <Link href={paths.home}>
                        <a className="ml-10 place-self-center text-xl md:text-2xl lg:text-4xl text-fill-900 font-medium hover:font-bold hover:underline">
                            Home
                        </a>
                    </Link>
                </div>
                <p className="place-self-center text-lg md:text-xl lg:text-2xl font-medium leading-tight text-gray-700 pr-4">Hackathon
                    2022</p>
            </div>

            {children}
        </div>
    </>
})

export default Layout
