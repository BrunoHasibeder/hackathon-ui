import type {NextPage} from 'next'
import {observer} from "mobx-react"
import {useRouter} from "next/router"
import {useEffect, useState} from "react"
import paths from "../util/paths";

const Home: NextPage = observer(() => {
    const router = useRouter()

    const [secRemaing, setSecRemaining] = useState(3)

    const target = paths.filliFutureProduct

    useEffect(() => {
        router.replace(target)

        let remaining = 3
        const interval = setInterval(() => {
            remaining--

            setSecRemaining(remaining)
        }, 1000)

        return () => clearInterval(interval)
    }, [])

    return <main>
        <div className="flex flex-col space-y-6 p-8 w-full">
            <div className="flex flex-row w-full place-content-center">
                <p className="w-fit text-4xl font-medium text-[#b60000]">Please wait while being redirected...</p>
            </div>
            <div className="flex flex-row w-full place-content-center">
                <p className="text-lg">
                    {secRemaing > 0 ?
                        <>Redirecting in <span className="text-[#b60000]">{secRemaing} seconds</span></> :
                        <>If nothing happens, click <button className="text-[#b60000] underline"
                                                            onClick={e => router.push(target)}>here</button></>
                    }
                </p>
            </div>
        </div>
    </main>
})

export default Home
