import React, {useEffect, useState} from 'react';
import OptionsSelect from '../../components/OptionsSelect';
import RatingStars from '../../components/RatingStars';
import Slideshow from '../../components/Slideshow';
import {useRouter} from "next/router";
import paths from "../../util/paths";
import Head from "next/head";
import {Card} from "@material-tailwind/react";
import axios from "axios";
import config from "../../config";

const FilliFuture: React.FC = () => {
    const router = useRouter()

    const [variant, setVariant] = useState(0)
    const [ratings, setRatings] = useState([])

    useEffect(() => {
        const updateData = async () => {
            const data = await axios.get(`http://${config().backendIP}:8000/ratings`).then((r) => {
                const data = r.data
                return data
            })
            setRatings(data)
        }

        const interval = setInterval(() => {
            updateData()
        }, 1000)

        return () => clearInterval(interval)
    }, [])

    function average(ratings: { product: number, stars: number, comment: string }[]): number {
        let sum = 0
        for (const entry of ratings) {
            sum += entry.stars
        }
        return Math.round(sum * 100 / (ratings.length ? ratings.length : 1)) / 100
    }

    return <main className="p-4">
        <Head>
            <title>FILLer | Filli Future</title>
        </Head>

        <Card className="p-4">
            <div className='filli-future'>
                <Slideshow
                    imageSrcs={getImages(variant)}></Slideshow>
                <div className="product-information">
                    <h1 className="product-header">{getProductName(variant)}</h1>
                    <RatingStars rating={average(ratings)} ratingsAmount={ratings.length}></RatingStars>
                    <div className='description flex flex-col space-y-2'>
                        <p>The Filli Future model is the ideal present for you or your children!</p>
                        <p>
                            It comes in two variants featuring a unique design and characteristics. All variants possess
                            rotatable joints providing you and your family extra fun. Order now!
                        </p>
                        {variant === 0 ?
                            <p>
                                The standard edition is a classic and comes with the following goodies:
                                <li className="mt-2">Rotatable head</li>
                                <li>Rotatable hands</li>
                                <li>Rotatable legs</li>
                                <li>Original and familiar colors</li>
                            </p> :
                            <p>
                                The ninja edition is a brand new design equipped with the following goodies:
                                <li className="mt-2">Rotatable head</li>
                                <li>Rotatable hands</li>
                                <li>Rotatable legs</li>
                                <li>Focking ninja sword</li>
                            </p>}
                    </div>
                    <hr></hr>

                    <OptionsSelect options={["Standard", "Ninja-Edition"]} onSelect={(index) => setVariant(index)}/>

                    <button className='order-button' onClick={e => {
                        router.push(paths.newOrder(variant))
                    }}>Order
                    </button>
                </div>
            </div>
        </Card>
    </main>
}

export default FilliFuture

function getProductName(productType: number) {
    switch (productType) {
        case 0:
            return "Filli Future (Standard)"
        case 1:
            return "Filli Future (Ninja)"
        default:
            return "Unknown Product"
    }
}

export function getImages(productType: number): string[] {
    switch (productType) {
        case 0:
            return ["/ori.png"]
        case 1:
            return ["/nin.png"]
        default:
            return []
    }
}