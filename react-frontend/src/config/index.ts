import rawDefaultConfig from "./config.json"

const defaultConfig = rawDefaultConfig as FrontendConfig
export {defaultConfig}
type FrontendConfig = {
    backendIP: string,
    ftsIP: string
}

export default function config() {
    return defaultConfig
}
