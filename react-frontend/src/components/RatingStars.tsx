import * as React from 'react';

interface Props {
  rating: number,
  ratingsAmount: number
}

const RatingStars: React.FunctionComponent<Props> = (props) => {
  return (
    <div className='rating'>
      <div className="rating-stars" style={{'--rating': props.rating} as React.CSSProperties}></div>
      <p className='rating-text'>{props.rating} {"(" + props.ratingsAmount + " reviews)"}</p>
    </div>
  );
};

export default RatingStars;