import React, {useState} from 'react';

interface Props {
    imageSrcs: string[];
}

const Slideshow: React.FC<Props> = (props) => {
    const [position, setPos] = useState(0);
    return (
        <div className="slideshow-container">
            <div className="mySlides fade">
                <div className='arrow arrow-left' onClick={()=> setPos(position > 0 ? (position - 1) % props.imageSrcs.length : props.imageSrcs.length - 1)}></div>
                <div className='arrow arrow-right' onClick={()=> setPos((position + 1) % props.imageSrcs.length)}></div>
                <div className='image-container' style={{transform: `translateX(${position * -400}px)`}}>
                {props.imageSrcs.map((img, index) => (
                    <img key={index} src={img} alt={`image ${index}`} className={`${index == position ? "selected" : ""}`}></img>
                ))}
                </div>
            </div>
            <div className='dots'>
                {props.imageSrcs.map((_, index) => (
                    <div key={index} className={`dot-container ${index == position ? "selected" : ""}`} onClick={()=>setPos(index)}>
                        <div className="dot"></div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Slideshow;