import React, {useEffect, useState} from 'react';

interface Props {
    options: string[];
    onSelect?: (option: number) => void;
}

const OptionsSelect: React.FC<Props> = (props) => {
    const [selected, setSelected] = useState(0);

    let selectItem = (index: number) => {
        if (props.onSelect)
            props.onSelect!(index)
    }

    useEffect(() => {
        selectItem(selected)
    }, [selected])

    return (
        <div className='options-select'>
            {props.options.map((option, index) => (
                <div onClick={() => setSelected(index)} key={index}
                     className={`select-item ${index == selected ? "selected" : ""}`}>{option}</div>
            ))}
        </div>
    );
};

export default OptionsSelect;