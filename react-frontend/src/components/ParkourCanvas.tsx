import {observer} from "mobx-react";
import {useEffect, useRef, useState} from "react";

const lines = [{
    x: 105,
    y: 0
}, {
    x: 105,
    y: 150
}, {
    x: 300,
    y: 150
}, {
    x: 300,
    y: 40
}, {
    x: 500,
    y: 40
}, {
    x: 500,
    y: 710
}, {
    x: 960,
    y: 710
}, {
    x: 960,
    y: 550
}]

const containers = [{
    x: 465,
    y: 50
}, {
    x: 820,
    y: 800
}]

let tempLength = 0
let prev = lines[0]
for (let i = 1; i < lines.length; i++) {
    const line = lines[i]
    tempLength += Math.sqrt((prev.x - line.x) * (prev.x - line.x) + (prev.y - line.y) * (prev.y - line.y))
    prev = line
}
const length = tempLength

const pd = 50

const ParkourCanvas = observer((
    {progress, container}:
        { progress: number, container: number | null }
) => {
    const canvasRef = useRef<HTMLCanvasElement>(null)

    const [parkourImage, setParkourImage] = useState(new Image())
    const [parkourImageLoaded, setParkourImageLoaded] = useState(false)
    const [carImage, setCarImage] = useState(new Image())
    const [carImageLoaded, setCarImageLoaded] = useState(false)

    const draw = (ctx: CanvasRenderingContext2D) => {
        ctx.drawImage(parkourImage, pd, pd, 1000, 750)
        ctx.strokeStyle = "#dddddd"
        ctx.lineWidth = 10
        ctx.lineCap = "round"

        ctx.beginPath()
        let prev = lines[0]
        ctx.moveTo(prev.x + pd, prev.y + pd)
        for (let i = 1; i < lines.length; i++) {
            const line = lines[i]
            ctx.lineTo(line.x + pd, line.y + pd)
        }
        ctx.stroke()

        ctx.strokeStyle = "#b90000"
        ctx.beginPath()
        ctx.moveTo(prev.x + pd, prev.y + pd)
        let marchedLength = 0
        let end = {x: 0, y: 0}
        let rotation = 0
        const targetLength = length * Math.min(progress, 1)
        for (let i = 1; i < lines.length; i++) {
            const line = lines[i]
            const length = Math.sqrt((prev.x - line.x) * (prev.x - line.x) + (prev.y - line.y) * (prev.y - line.y))

            if (marchedLength + length >= targetLength) {
                const t = (targetLength - marchedLength) / length
                end = {
                    x: line.x * t + prev.x * (1 - t),
                    y: line.y * t + prev.y * (1 - t)
                }

                ctx.lineTo(end.x + pd, end.y + pd)

                const dy = line.y - prev.y
                const dx = line.x - prev.x
                rotation = Math.atan(dy / dx)
                break
            }

            marchedLength += length
            ctx.lineTo(line.x + pd, line.y + pd)

            prev = line
        }
        ctx.stroke()

        ctx.fillStyle = "#ff0000"
        ctx.strokeStyle = "#b90000"
        ctx.lineWidth = 5
        containers.forEach((val, index) => {
            ctx.beginPath()
            ctx.rect(val.x - 25, val.y - 25, 50, 50)
            ctx.fill()
            ctx.stroke()

            if (index == container) {
                const prevStyle = ctx.fillStyle
                ctx.fillStyle = "#f1dcdc"
                ctx.fillRect(val.x - 12.5, val.y - 12.5, 25, 25)
                ctx.fillStyle = prevStyle
            }
        })

        ctx.save()

        ctx.translate(end.x + 50, end.y + 50)
        ctx.rotate(rotation)
        ctx.translate(-75, -75)
        ctx.drawImage(carImage, 0, 0, 150, 150)
        ctx.restore()

        ctx.fillStyle = "#000000"
        ctx.font = "bold 24px sans-serif"
        const text = `${Math.min(Math.round(progress * 100), 100)}%`
        const textDim = ctx.measureText(text)
        ctx.fillText(text, end.x + 50 - textDim.width / 2, end.y + 50 + 16 / 2)
    }

    useEffect(() => {
        parkourImage.src = "/parkour.png"
        parkourImage.onload = () => {
            setParkourImageLoaded(true)
        }

        carImage.src = "/car.png"
        carImage.onload = () => {
            setCarImageLoaded(true)
        }
    })

    useEffect(() => {
        if (parkourImageLoaded && carImageLoaded) {
            const canvas = canvasRef.current!!
            const context = canvas.getContext('2d')!!

            context.clearRect(0, 0, canvas.width, canvas.height)
            draw(context)
        }
    }, [progress, container, parkourImageLoaded, carImageLoaded])


    return <canvas ref={canvasRef} height={850} width={1100} className="w-full 2xl:w-9/12 4xl:w-6/12"/>
})

export default ParkourCanvas